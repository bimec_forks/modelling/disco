.. _upgrade_cost_analysis_schemas:

**********************************
Upgrade Cost Analysis JSON Schemas
**********************************

UpgradeCostAnalysisSimulationModel
==================================
.. literalinclude:: ../../build/json_schemas/UpgradeCostAnalysisSimulationModel.json
    :language: json

UpgradeSummaryResultsModel
==========================
.. literalinclude:: ../../build/json_schemas/UpgradeSummaryResultsModel.json
    :language: json
